package tw.monsta.dbip;

import org.apache.http.HttpStatus;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

/**
 * Created by johnson on 2016/8/9.
 */
public class TestGetKeyInfo {

    private static final String API_KEY = "933d623d11d87ef7d1979466915930d31770eae4";
    private static final String API_BASE_URL = "http://api.db-ip.com/v2/";

    @Test
    public void getKeyInfo() {
        given().log().all().
        when()
                .get(API_BASE_URL + API_KEY).
        then()
                .log().all()
                .statusCode(HttpStatus.SC_OK)
                .body("apiKey", equalTo(API_KEY))
                .body("queriesPerDay", equalTo(2500))
                .body("queriesLeft", notNullValue())
                .body("expires", notNullValue());
    }

    @Test
    public void getIPAddressInfo() {
        when()
                .get(API_BASE_URL + API_KEY + "/8.8.8.8").
        then()
                .log().all()
                .statusCode(HttpStatus.SC_OK)
                .body("countryCode", equalTo("US"));
    }

    @Test
    public void withWrongAPIKey_shouldResponseStatusOK_andErrorMsg() {
        when()
                .get(API_BASE_URL + "12345").
        then()
                .log().all()
                .statusCode(HttpStatus.SC_OK)
                .body("error", notNullValue());
    }

    @Test
    public void getAPIAddressInfo_withInvalidIP_shouldResponseWithErrorMsg() {
        when()
                .get(API_BASE_URL + API_KEY + "/140.119.0.999").
        then()
                .log().all()
                .statusCode(HttpStatus.SC_OK)
                .body("error", notNullValue());
    }
}
